'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BASE_API: '"https://easy-mock.com/mock/5950a2419adc231f356a6636/vue-admin"',
  BACK_END_API: {
    // 生产主机域名
    BUSINESS: 'https://www.fanlihuitou.com/flht/v1/prod',
    IMG: 'https://www.fanlihuitou.com/flht/v1/msg',
    ACTIVE: 'https://www.fanlihuitou.com/flht/v1/crm',
    ORDER:'https://www.fanlihuitou.com/flht/v1/partner',
    pay:'https://www.fanlihuitou.com/flht/v1/pay',
    auth:'https://www.fanlihuitou.com/flht/v1/auth'
  }
}
