'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"https://easy-mock.com/mock/5950a2419adc231f356a6636/vue-admin"',
  BACK_END_API: {
     // 测试主机域名
     BUSINESS: 'https://www.fanlihuitou8.com/flht/v1/prod',
     IMG: 'https://www.fanlihuitou8.com/flht/v1/msg',
     ACTIVE: 'https://www.fanlihuitou8.com/flht/v1/crm',
     ORDER:'https://www.fanlihuitou8.com/flht/v1/partner',
     pay:'https://www.fanlihuitou8.com/flht/v1/pay',
     auth:'https://www.fanlihuitou8.com/flht/v1/auth'
  }
})
