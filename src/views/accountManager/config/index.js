// 非汉字校验
const notNullCheck = (rule, value, callback) => {
  if (value === '' || value === null || value === undefined) {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (value === '' || value === null || value === undefined) {
      callback(new Error('亲， 该字段不能为空'))
    } else {
      callback()
    }
  }, 300)
}

const rules = { // 验证规则
  checkStatus: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  checkUserDesc: [
    { required: true, message: '请输入活动描述', trigger: 'blur' },
    { min: 0, max: 300, message: '不超过三百个字', trigger: 'blur' }
  ]
}

export {
  rules
}
