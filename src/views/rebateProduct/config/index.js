// url的校验
const urlCheck = (rule, value, callback) => {
  const regExp = /[\u4e00-\u9fa5]/
  const match = /^((ht|f)tps?):\/\/[\w\-]+(\.[\w\-]+)+([\w\-\.,@?^=%&:\/~\+#]*[\w\-\@?^=%&\/~\+#])?$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === true) {
      callback(new Error('亲，请勿输入汉字'))
    } else if (match.test(value) === false) {
      callback(new Error('请输入正确的url格式'))
    } else {
      callback()
    }
  }, 300)
}
// 非汉字校验
const integerCheck = (rule, value, callback) => {
  const regExp = /^[1-9]\d*$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入正整数'))
    } else {
      callback()
    }
  }, 300)
}

// 非汉字校验
const double2Check = (rule, value, callback) => {
  const regExp = /^[0-9]+(.[0-9]{1,2})?$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入整数或两位小数'))
    } else {
      callback()
    }
  }, 300)
}

// 非汉字校验
const double2CheckOrNull = (rule, value, callback) => {
  const regExp = /^[0-9]+(.[0-9]{1,2})?$/
  if (value === '' || value === null || value === undefined) {
    callback()
  }
  setTimeout(() => {
    console.log('double2CheckOrNull value = ' + value)
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入整数或两位小数'))
    } else {
      callback()
    }
  }, 300)
}
// 非汉字校验
const notNullCheck = (rule, value, callback) => {
  if (value === '' || value === null || value === undefined) {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (value === '' || value === null || value === undefined) {
      callback(new Error('亲， 该字段不能为空'))
    } else {
      callback()
    }
  }, 300)
}

const rules = { // 验证规则
  name: [
    { required: true, message: '请输入活动名称', trigger: 'blur' },
    { min: 0, max: 20, message: '不超过二十个字', trigger: 'blur' },
    { type: 'string', message: '必须字符串', trigger: 'blur' }
  ],
  companyUrl: [
    { validator: urlCheck, trigger: 'blur' }
  ],
  jumpUrl: [
    { validator: urlCheck, trigger: 'blur' }
  ],
  logoUrl: [
    { validator: urlCheck, trigger: 'blur' }
  ],
  maxProfit: [
    { validator: double2Check, trigger: 'blur' }
  ],
  profit: [
    { validator: double2Check, trigger: 'blur' }
  ],
  minYearProfit: [
    { validator: double2Check, trigger: 'blur' }
  ],
  maxYearProfit: [
    { validator: double2Check, trigger: 'blur' }
  ],
  investmentNum: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  operateTerm: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  profitOrigin: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  payBackTerm: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  companyName: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  remark: [
    { required: true, message: '请输入活动描述', trigger: 'blur' },
    { min: 0, max: 300, message: '不超过三百个字', trigger: 'blur' }
  ],
  subscriptionOrigin: [
    { validator: double2Check, trigger: 'blur' }
  ],
  projectType: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  productNo: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  term: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  prodProfit: [
    { validator: double2Check, trigger: 'blur' }
  ],
  profitPercent: [
    { validator: double2Check, trigger: 'blur' }
  ],
  allInRate: [
    { validator: double2Check, trigger: 'blur' }
  ],
  extraDate: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  prodProfitPercent: [
    { validator: double2CheckOrNull, trigger: 'blur' }
  ],
  profitPercentMax: [
    { validator: double2CheckOrNull, trigger: 'blur' }
  ],
  extraProfit: [
    { validator: double2CheckOrNull, trigger: 'blur' }
  ]
}

export {
  rules
}
