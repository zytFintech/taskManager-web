// 整数校验
const integerCheck = (rule, value, callback) => {
  const regExp = /^[1-9]\d*$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入正整数'))
    } else {
      callback()
    }
  }, 300)
}

// 2位小数校验
const double2Check = (rule, value, callback) => {
  const regExp = /^[0-9]+(.[0-9]{1,2})?$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入整数或两位小数'))
    } else {
      callback()
    }
  }, 300)
}

// 非汉字校验
const notNullCheck = (rule, value, callback) => {
  if (value === '' || value === null) {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (value === '' || value === null) {
      callback(new Error('亲， 该字段不能为空'))
    } else {
      callback()
    }
  }, 300)
}
// 2位小数校验
const phoneNumCheck = (rule, value, callback) => {
  const regExp = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/
  if (value === '') {
    return callback(new Error('该字段不能为空'))
  }
  setTimeout(() => {
    if (regExp.test(value) === false) {
      callback(new Error('亲，请输入正确的手机号'))
    } else {
      callback()
    }
  }, 300)
}

const rules = { // 验证规则
  orderIdPartner: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  orderTime: [
    { validator: notNullCheck, trigger: 'blur' }
  ],
  userMobile: [
    { validator: phoneNumCheck, trigger: 'blur' }
  ],
  productTerm: [
    { validator: integerCheck, trigger: 'blur' }
  ],
  productRealPayFee: [
    { validator: double2Check, trigger: 'blur' }
  ],
  productCommission: [
    { validator: double2Check, trigger: 'blur' }
  ]
}

export {
  rules
}
