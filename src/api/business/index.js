import axios from 'axios'

const api = {
  companyDetail: '/flht/v1/prod/company/detail',
  companyList: '/flht/v1/prod/company/list',
  companySave: '/flht/v1/prod/company/save',
  companyDelete: '/flht/v1/prod/company/delete',
  companyLabelList: '/flht/v1/prod/company/label/list',
  companyUpdateCompanyLabel: '/flht/v1/prod/company/label/update',
  companyLabelAdd: '/flht/v1/prod/company/label/add',
  companyLabelDelete: '/flht/v1/prod/company/label/delete',
  companySelectList: '/flht/v1/prod/company/select/list',
  companyApprove: '/flht/v1/prod/company/approve',
  companySale: '/flht/v1/prod/company/sale',
  partnerAdd: '/flht/v1/partner/crm/partner/add',
  partnerUpdate: '/flht/v1/partner/crm/partner/update',
  partnerGet: '/flht/v1/partner/crm/partner/getByCompanyId'
}
// 获取商家列表
const companyList = (parm) => {
  return axios.get(api.companyList + '?pageSize=' + parm.pageSize + '&curPage=' + parm.curPage + '&companyName=' + parm.companyName +
    '&profitOrigin=' + parm.profitOrigin + '&saleStatus=' + parm.saleStatus)
}
// 查询商家基本信息
const companyDetail = (parm) => {
  return axios.get(api.companyDetail + '?companyId=' + parm)
}
// 增加/修改商家信息
const companySave = (parm) => {
  return axios.post(api.companySave, parm)
}
// 删除商家
const companyDelete = (parm) => {
  return axios.get(api.companyDelete + '?companyId=' + parm)
}
// 商家标签列表
const companyLabelList = (companyId) => {
  return axios.get(api.companyLabelList + '?companyId=' + companyId)
}
// 修改商家标签
const companyUpdateCompanyLabel = (parm) => {
  return axios.post(api.companyUpdateCompanyLabel, parm)
}

const companyLabelAdd = (parm) => {
  return axios.post(api.companyLabelAdd, parm)
}

const companyLabelDelete = (companyLabelId) => {
  return axios.get(api.companyLabelDelete + '?companyLabelId=' + companyLabelId)
}

const companySelectList = () => {
  return axios.get(api.companySelectList)
}

const companyApprove = (parm) => {
  return axios.post(api.companyApprove + '?approveType=' + parm.approveType + '&companyId=' + parm.companyId)
}

const companySale = (parm) => {
  console.log(parm)
  return axios.post(api.companySale + '?companyId=' + parm.companyId + '&saleStatus=' + parm.saleStatus)
}

const partnerAdd = (parm) => {
  console.log(parm)
  return axios.post(api.partnerAdd, parm)
}

const partnerUpdate = (parm) => {
  console.log(parm)
  return axios.post(api.partnerUpdate, parm)
}

const partnerGet = (parm) => {
  console.log(parm)
  return axios.get(api.partnerGet + '?companyId=' + parm)
}

export {
  api,
  companyDetail,
  companyList,
  companySave,
  companyDelete,
  companyLabelList,
  companyUpdateCompanyLabel,
  companyLabelAdd,
  companyLabelDelete,
  companySelectList,
  companyApprove,
  companySale,
  partnerAdd,
  partnerUpdate,
  partnerGet
}
