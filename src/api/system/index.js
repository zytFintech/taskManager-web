import axios from 'axios'

const api = {
  createRole: '/flht/v1/auth/crm/userRole/create',
  roleList: '/flht/v1/auth/crm/userRole/list',
  reviseRole: '/flht/v1/auth/crm/userRole',
  deleteRole: '/flht/v1/auth/crm/userRole',
  checkRoleDetail: '/flht/v1/auth/crm/userRole',
  userList: '/flht/v1/auth/crm/visitors'
}

const createRole = (parm) => {
  return axios.post(api.createRole, parm)
}

const roleList = (parm) => {
  return axios.get(api.roleList + '?keyword=' + parm.roleName + '&curPage=' + parm.curPage + '&pageSize=' + parm.pageSize)
}

const reviseRole = (id, parm) => {
  return axios.post(api.reviseRole + '/' + id, parm)
}

const deleteRole = (id) => {
  return axios.post(api.deleteRole + '/' + id + '/delete')
}

const checkRoleDetail = (id) => {
  return axios.get(api.checkRoleDetail + '/' + id)
}

const checkUserRole = (userId) => {
  return axios.get(api.userRoleAssign + '?userId=' + userId)
}

const getUserList = (parm) => {
  return axios.get(api.userList + '?phoneNum=' + parm.phoneNum + '&pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

export {
  api,
  createRole,
  roleList,
  reviseRole,
  deleteRole,
  checkRoleDetail,
  checkUserRole,
  getUserList
}
