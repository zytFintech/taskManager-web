import axios from 'axios'

const api = {
  couponList: '/flht/v1/crm/coupon/list',
  couponSave: '/flht/v1/crm/coupon/save',
  couponDelete: '/flht/v1/crm/coupon/delete'
}

const couponList = (parm) => {
  return axios.get(api.couponList + '?pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

const couponSave = (parm) => {
  return axios.post(api.couponSave, parm)
}

const couponDelete = (cardId) => {
  return axios.get(api.couponDelete + '?cardId=' + cardId)
}

export {
  api,
  couponList,
  couponSave,
  couponDelete
}
