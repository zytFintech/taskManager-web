import axios from 'axios'

const api = {
  orderList: '/flht/v1/partner/order/all',
  orderExcel: '/flht/v1/partner/order/excel',
  orderPartnerAll: '/flht/v1/partner/order/partner/all',
  orderPartnerCheck: '/flht/v1/partner/order/partner/check',
  orderPartnerSave: '/flht/v1/partner/order/partnerOrder/save'
}

const orderList = (parm) => {
  return axios.get(api.orderList + '?curPage=' + parm.curPage + '&pageSize=' + parm.pageSize + '&phoneNum=' + parm.phoneNum + '&retailerName=' + parm.retailerName + '&startTime=' + parm.startTime + '&endTime=' + parm.endTime)
}

const orderExcel = (parm) => {
  return axios.get(api.orderExcel)
}

const orderPartnerAll = (parm) => {
  return axios.get(api.orderPartnerAll + '?curPage=' + parm.curPage + '&pageSize=' + parm.pageSize + '&phoneNum=' + parm.phoneNum + '&startTime=' + parm.startTime + '&endTime=' + parm.endTime)
}

const orderPartnerCheck = (parm) => {
  return axios.get(api.orderPartnerCheck + '?orderIdPartner=' + parm.orderIdPartner + '&companyNo=' + parm.companyNo + '&status=' + parm.status)
}

// 新增/修改产品
const orderPartnerSave = (parm) => {
  return axios.post(api.orderPartnerSave, parm)
}

export {
  api,
  orderList,
  orderExcel,
  orderPartnerAll,
  orderPartnerCheck,
  orderPartnerSave
}
