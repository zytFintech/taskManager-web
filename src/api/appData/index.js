import axios from 'axios'

const api = {
  pointList: '/flht/v1/msg/point/list',
  typePoint: '/flht/v1/msg/type/point'
}

const pointList = (parm) => {
  return axios.get(api.pointList + '?userPhone=' + parm.userPhone + '&startDay=' + parm.startDay + '&endDay=' + parm.endDay + '&curPage=' + parm.curPage + '&pageSize=' + parm.pageSize + '&pointType=' + parm.pointType)
}

const typePoint = (parm) => {
  return axios.get(api.typePoint)
}

export {
  api,
  pointList,
  typePoint
}
