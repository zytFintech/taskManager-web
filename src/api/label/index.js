import axios from 'axios'

const api = {
  labelGroupList: '/flht/v1/prod/labelGroup/list',
  labelGroupAdd: '/flht/v1/prod/labelGroup/add',
  labelGroupDelete: '/flht/v1/prod/labelGroup/delete',
  labelGroupDeatil: '/flht/v1/prod/labelGroup/detail',
  labelGroupUpdate: '/flht/v1/prod/labelGroup/update',
  labelList: '/flht/v1/prod/label/list',
  labelAdd: '/flht/v1/prod/label/add',
  labelDelete: '/flht/v1/prod/label/delete',
  labelDetail: '/flht/v1/prod/label/detail',
  labelUpdate: '/flht/v1/prod/label/update',
  labelTotal: '/flht/v1/prod/label/total'
}

// 获取标签组列表
const labelGroupList = (parm) => {
  return axios.get(api.labelGroupList + '?curPage=' + parm.curPage + '&pageSize=' + parm.pageSize + '&labelGroupName=' + parm.labelGroupName)
}
// 获取标签组详情
const labelGroupDeatil = (labelGroupId) => {
  return axios.get(api.labelGroupDeatil + '?labelGroupId=' + labelGroupId)
}
// 增加标签组
const labelGroupAdd = (parm) => {
  return axios.post(api.labelGroupAdd, parm)
}
// 删除标签组
const labelGroupDelete = (labelGroupId) => {
  return axios.get(api.labelGroupDelete + '?labelGroupId=' + labelGroupId)
}
// 获取标签项列表 根据标签组
const labelList = (labelGroupId) => {
  return axios.get(api.labelList + '?labelGroupId=' + labelGroupId)
}
// 修改标签组
const labelGroupUpdate = (parm) => {
  return axios.post(api.labelGroupUpdate, parm)
}
// 新增标签项
const labelAdd = (parm) => {
  return axios.post(api.labelAdd, parm)
}
// 删除标签项
const labelDelete = (labelId) => {
  return axios.get(api.labelDelete + '?labelId=' + labelId)
}
// 查看标签详情
const labelDetail = (labelId) => {
  return axios.get(api.labelDetail + '?labelId=' + labelId)
}
// 修改标签项
const labelUpdate = (parm) => {
  return axios.post(api.labelUpdate, parm)
}

const labelTotal = () => {
  return axios.get(api.labelTotal)
}

export {
  api,
  labelGroupList,
  labelGroupAdd,
  labelGroupDelete,
  labelGroupDeatil,
  labelGroupUpdate,
  labelList,
  labelAdd,
  labelDelete,
  labelDetail,
  labelUpdate,
  labelTotal
}
