import axios from 'axios'

const api = {
  login: '/flht/v1/auth/crm/visitor/login'
}

const login = (parm) => {
  return axios.post(api.login, parm)
}

export {
  api,
  login
}
