import axios from 'axios'

const api = {
  retailersList: '/flht/v1/prod/retailers/list',
  retailersSelectList: '/flht/v1/prod/crm/retailers/list',
  retailersSave: '/flht/v1/prod/retailers/save',
  retailersDetail: '/flht/v1/prod/retailers/detail',
  retailersGetNo: '/flht/v1/prod/crm/retailers/no'
}

const retailersList = (parm) => {
  return axios.get(api.retailersList + '?pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

const retailersSelectList = () => {
  return axios.get(api.retailersSelectList)
}

const retailersSave = (parm) => {
  return axios.post(api.retailersSave, parm)
}

const retailersDetail = (retailersId) => {
  return axios.get(api.retailersDetail + '?retailersId=' + retailersId)
}

const retailersGetNo = (retailersName) => {
  return axios.get(api.retailersGetNo + '?retailersName=' + retailersName)
}

export {
  api,
  retailersList,
  retailersSelectList,
  retailersSave,
  retailersDetail,
  retailersGetNo
}
