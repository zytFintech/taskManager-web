import axios from 'axios'

const api = {
  getActivityList: '/flht/v1/crm/activity/all',
  activitySave: '/flht/v1/crm/activity/save',
  getactivityDetail: '/flht/v1/crm/activity'
}

const getActivityList = (parm) => {
  return axios.get(api.getActivityList)
}

const activitySave = (parm) => {
  return axios.post(api.activitySave, parm)
}

const getactivityDetail = (parm) => {
  return axios.get(api.getactivityDetail + '/' + parm)
}

export {
  api,
  getActivityList,
  activitySave,
  getactivityDetail
}
