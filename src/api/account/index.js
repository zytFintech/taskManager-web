import axios from 'axios'

const api = {
  userList: '/flht/v1/auth/crm/users',
  userExcel: '/flht/v1/auth/crm/users/excel',
  accountFlowAll: '/flht/v1/pay/crm/accountflow/all',
  userGetByPhoneNum: '/flht/v1/auth/crm/users/getByPhoneNum',
  userGetByUserId: '/flht/v1/auth/crm/users/getByUserId',
  withDrawCheckAll: '/flht/v1/pay/crm/withDrawCheck/all',
  withDrawCheckUpdStatus: '/flht/v1/pay/crm/withDrawCheck/approve'
}

const getUserList = (parm) => {
  return axios.get(api.userList + '?phoneNum=' + parm.phoneNum + '&retailersName=' + parm.retailersName + '&pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

const getAccountFlowAll = (parm) => {
  return axios.get(api.accountFlowAll + '?phoneNum=' + parm.phoneNum + '&queryStart=' + parm.queryStart + '&queryEnd=' + parm.queryEnd + '&pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

const userGetByPhoneNum = (phoneNum) => {
  return axios.get(api.userGetByPhoneNum + '?phoneNum=' + phoneNum)
}

const userGetByUserId = (userId) => {
  return axios.get(api.userGetByUserId + '?userId=' + userId)
}

const withDrawCheckAll = (parm) => {
  return axios.post(api.withDrawCheckAll, parm)
}

const withDrawCheckUpdStatus = (parm) => {
  return axios.post(api.withDrawCheckUpdStatus, parm)
}

export {
  api,
  getUserList,
  userGetByPhoneNum,
  getAccountFlowAll,
  withDrawCheckAll,
  withDrawCheckUpdStatus,
  userGetByUserId
}
