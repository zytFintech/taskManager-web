import axios from 'axios'

const api = {
  visitorsList: '/flht/v1/auth/crm/visitors',
  visitorAdd: '/flht/v1/auth/crm/visitor',
  updateVisitor: '/flht/v1/auth/crm/visitors/update',
  visitorInitPwd: '/flht/v1/auth/crm/visitors/initPwd'
}

const getVisitorsList = (parm) => {
  return axios.get(api.visitorsList + '?phoneNum=' + parm.phoneNum + '&pageSize=' + parm.pageSize + '&curPage=' + parm.curPage)
}

const updateVisitor = (parm) => {
  return axios.put(api.updateVisitor, parm)
}

const visitorAdd = (parm) => {
  return axios.post(api.visitorAdd, parm)
}

const visitorInitPwd = (userId) => {
  return axios.put(api.visitorInitPwd + '/' + userId)
}

export {
  api,
  getVisitorsList,
  visitorInitPwd,
  updateVisitor,
  visitorAdd
}
