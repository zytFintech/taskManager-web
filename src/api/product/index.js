import axios from 'axios'

const api = {
  productDelete: '/flht/v1/prod/product/delete',
  productSave: '/flht/v1/prod/product/save',
  productDetail: '/flht/v1/prod/product/detail',
  productList: '/flht/v1/prod/product/list',
  productApprove: '/flht/v1/prod/product/approve',
  productSale: '/flht/v1/prod/product/sale'
}
// 删除产品
const productDelete = (productId) => {
  return axios.get(api.productDelete + '?productId=' + productId)
}
// 新增/修改产品
const productSave = (parm) => {
  return axios.post(api.productSave, parm)
}
// 产品列表
const productList = (parm) => {
  return axios.get(api.productList + '?productType=' + parm.productType + '&companyId=' + parm.companyId + '&curPage=' + parm.curPage + '&pageSize' + parm.pageSize + '&investType=' + parm.investType)
}
// 获取产品详情
const productDetail = (productId) => {
  return axios.get(api.productDetail + '?productId=' + productId)
}
// 产品审核
const productApprove = (parm) => {
  return axios.post(api.productApprove + '?productId=' + parm.productId + '&approveType=' + parm.approveType)
}
// 产品上下架
const productSale = (parm) => {
  return axios.post(api.productSale + '?productId=' + parm.productId + '&seleType=' + parm.seleType)
}

export {
  api,
  productDelete,
  productSave,
  productList,
  productDetail,
  productApprove,
  productSale
}
