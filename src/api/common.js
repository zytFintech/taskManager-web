import axios from 'axios'

const api = {
  postUploadImg: '/flht/v1/msg/qiniu/upload/file' // 上传图片
}

const postUploadImg = (parm) => {
  console.log(parm)
  return axios.post(api.postUploadImg, parm)
}

export {
  api,
  postUploadImg
}
