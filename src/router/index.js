import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from 'views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: resolve => require(['views/dashboard/index'], resolve)
    }]
  },
  // 商家管理
  {
    path: '/business',
    name: 'business',
    redirect: '/business/businessList',
    component: Layout,
    meta: { title: '商家维护', icon: 'form' },
    children: [
      {
        path: 'businessList',
        name: 'businessList',
        component: resolve => require(['views/business/list'], resolve),
        meta: { title: '商家录入', icon: 'form' }
      },
      {
        path: 'businessAudit',
        name: 'businessAudit',
        component: resolve => require(['views/business/businessAudit'], resolve),
        meta: { title: '商家审核', icon: 'form' }
      },
      {
        path: 'businessConfig',
        name: 'businessConfig',
        component: resolve => require(['views/business/businessConfig'], resolve),
        meta: { title: '商家配置', icon: 'form' }
      },
      {
        path: 'addBusiness',
        name: 'addBusiness',
        component: resolve => require(['views/business/add'], resolve),
        meta: { title: '新增商家', icon: 'form' },
        hidden: true
      },
      {
        path: 'editBusiness',
        name: 'editBusiness',
        component: resolve => require(['views/business/edit'], resolve),
        meta: { title: '编辑商家', icon: 'form' },
        hidden: true
      }
    ]
  },
  // 推荐产品
  {
    path: '/introProduct',
    name: 'introProduct',
    component: Layout,
    redirect: '/introProduct/introProductList',
    meta: { title: '推荐产品', icon: 'form' },
    children: [
      {
        path: 'introProductList',
        name: 'introProductList',
        component: resolve => require(['views/introProduct/introList'], resolve),
        meta: { title: '新增推荐产品', icon: 'form' }
      },
      {
        path: 'productAudit',
        name: 'productAudit',
        component: resolve => require(['views/introProduct/introAudit'], resolve),
        meta: { title: '推荐产品审核', icon: 'form' }
      }
    ]
  },
  // 返利产品
  {
    path: '/rebateProduct',
    name: 'rebateProduct',
    component: Layout,
    redirect: '/rebateProduct/rebateProductList',
    meta: { title: '返利产品', icon: 'form' },
    children: [
      {
        path: 'rebateProductList',
        name: 'rebateProductList',
        component: resolve => require(['views/rebateProduct/rebateList'], resolve),
        meta: { title: '新增返利产品', icon: 'form' }
      },
      {
        path: 'rebateAudit',
        name: 'rebateAudit',
        component: resolve => require(['views/rebateProduct/rebateAudit'], resolve),
        meta: { title: '返利产品审核', icon: 'form' }
      }
    ]
  },
  // 标签管理
  {
    path: '/label',
    name: 'label',
    component: Layout,
    redirect: '/label/labelGroupList',
    meta: { title: '标签管理', icon: 'form' },
    children: [
      {
        path: 'labelGroupList',
        name: 'labelGroupList',
        component: resolve => require(['views/label/list'], resolve),
        meta: { title: '标签组列表', icon: 'form' }
      },
      {
        path: 'labelGroupEdit',
        name: 'labelGroupEdit',
        component: resolve => require(['views/label/edit'], resolve),
        meta: { title: '标签组编辑', icon: 'form' },
        hidden: true
      },
      {
        path: 'labelGroupAdd',
        name: 'labelGroupAdd',
        component: resolve => require(['views/label/add'], resolve),
        meta: { title: '标签组新增', icon: 'form' },
        hidden: true
      },
      {
        path: 'labelItemList',
        name: 'labelItemList',
        component: resolve => require(['views/label/item'], resolve),
        meta: { title: '标签项配置', icon: 'form' }
      }
    ]
  },
  // 营销管理
  {
    path: '/marketManager',
    name: 'marketManager',
    component: Layout,
    meta: { title: '营销管理', icon: 'form' },
    children: [
      {
        path: 'activity',
        name: 'activity',
        component: resolve => require(['views/activity'], resolve),
        meta: { title: '活动', icon: 'form' }
      },
      {
        path: 'coupon',
        name: 'coupon',
        component: resolve => require(['views/coupon'], resolve),
        meta: { title: '卡券', icon: 'form' }
      }
    ]
  },
  // 账户管理
  {
    path: '/accountManager',
    name: 'accountManager',
    component: Layout,
    meta: { title: '账户管理', icon: 'form' },
    children: [
      {
        path: 'userInfo',
        name: 'userInfo',
        component: resolve => require(['views/accountManager/userInfo'], resolve),
        meta: { title: '用户信息', icon: 'form' }
      },
      {
        path: 'accountFlow',
        name: 'accountFlow',
        component: resolve => require(['views/accountManager/accountFlow'], resolve),
        meta: { title: '账户流水', icon: 'form' }
      },
      {
        path: 'widthDrawCheck',
        name: 'widthDrawCheck',
        component: resolve => require(['views/accountManager/widthDrawCheck'], resolve),
        meta: { title: '提现审核', icon: 'form' }
      }
    ]
  },
  // 订单管理
  {
    path: '/orderManagement',
    name: 'orderManagement',
    component: Layout,
    meta: { title: '订单管理', icon: 'form' },
    children: [
      {
        path: 'orderRecord',
        name: 'orderRecord',
        component: resolve => require(['views/order/orderFlht'], resolve),
        meta: { title: '订单记录', icon: 'form' }
      },
      {
        path: 'orderVerify',
        name: 'orderVerify',
        component: resolve => require(['views/order/orderVerify'], resolve),
        meta: { title: '订单审核', icon: 'form' }
      }
    ]
  },
  // 电商管理
  {
    path: '/onlineManager',
    name: 'onlineManager',
    component: Layout,
    meta: { title: '电商管理', icon: 'form' },
    children: [
      {
        path: 'onlineList',
        name: 'onlineList',
        component: resolve => require(['views/onlineRetailers/list'], resolve),
        meta: { title: '电商列表', icon: 'form' }
      }
    ]
  },
  // 数据统计
  {
    path: '/appData',
    name: 'appData',
    component: Layout,
    meta: { title: '数据统计', icon: 'form' },
    children: [
      {
        path: 'appDataList',
        name: 'appDataList',
        component: resolve => require(['views/appData'], resolve),
        meta: { title: '数据统计列表', icon: 'form' }
      },
      {
        path: 'appDataListAdd',
        name: 'appDataListAdd',
        component: resolve => require(['views/appData'], resolve),
        meta: { title: '数据统计', icon: 'form' },
        hidden: true
      }
    ]
  },
  // 员工管理
  {
    path: '/employeeManager',
    name: 'employeeManager',
    component: Layout,
    meta: { title: '员工管理', icon: 'form' },
    children: [
      {
        path: 'employeeInfo',
        name: 'employeeInfo',
        component: resolve => require(['views/employeeManager/employeeInfo'], resolve),
        meta: { title: '员工信息', icon: 'form' }
      },
      {
        path: 'employeeInfo',
        name: 'employeeInfo',
        component: resolve => require(['views/employeeManager/employeeInfo'], resolve),
        meta: { title: '员工信息', icon: 'form' },
        hidden: true
      }
    ]
  },
  // 系统管理
  {
    path: '/systemManager',
    name: 'systemManager',
    component: Layout,
    meta: { title: '系统管理', icon: 'form' },
    children: [
      {
        path: 'roleManager',
        name: 'roleManager',
        component: resolve => require(['views/systemManager/roleManager'], resolve),
        meta: { title: '角色管理', icon: 'form' }
      },
      {
        path: 'roleManager',
        name: 'roleManager',
        component: resolve => require(['views/systemManager/roleManager'], resolve),
        meta: { title: '角色管理', icon: 'form' },
        hidden: true
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

