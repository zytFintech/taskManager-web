const handlePagination = {
  data() {
    return {
      pageSize: 10,
      curPage: 1,
      size: 0
    }
  },
  methods: {
    handleSizeChange(val) {
      this.pageSize = val
      this.getList()
    },
    handleCurrentChange(val) {
      this.curPage = val
      this.getList()
    }
  }
}

export default handlePagination
